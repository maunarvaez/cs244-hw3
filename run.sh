#!/bin/sh

if [ $# < 1 ]
then
	echo "Wrong number of arguments. To see the help page pass in 'h' as the first argument"
	exit
fi
 
if [ "$1" = "h" ]
then
	echo $1
	echo "Jellyfish script usage:\n\tTo run with default parameters use 'sh run.sh d' otherwise follow the help below and edit the line with EDIT" 
	python main.py -h
	exit
fi 

echo "Running this script will delete all the text files and png files in this directory and create new ones. Do you want to proceed? [y/n]"

read answer 

if [ "$answer" = "y" ]
then
	#rm *.txt *png
	if [ "$1" = "d" ]
	then
		python main.py 
	elif [ "$1" = "n" ]
	then
		python main.py -a True -i 5 -t 100
		if [ ! -f "Betweenness.png" ]
		then
			files=`ls betwc_*_results.txt`
			python plot_b.py $files 
		fi

	elif [ "$1" = "s" ]
	then
		python main.py -a True -t 10 -i 5 -n 100 -s 20 -r 2
		if [ ! -f "Betweenness.png" ]
                then
                        files=`ls betwc_*_results.txt`
                        python plot_b.py $files
                fi
	else
		python main.py -h #EDIT TO CHANGE THE INPUT PARAMETERS
	fi
	
fi


